import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from "@angular/http";
import { FormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { NavmenuComponent } from './components/navmenu/navmenu.component';
import { PhotosComponent } from './components/photos/photos.component';
import { GraphService } from "./services/graph.service";
import { TypicodeService } from "./services/typicode.service";
import { AuthService } from "./services/auth.service";
import { LoginComponent } from './components/login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavmenuComponent,
    PhotosComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot([
      { path: "", redirectTo: "vehicles", pathMatch: "full" },
      { path: "posts", component: HomeComponent },
      { path: "photos", component: PhotosComponent },
      { path: "login", component: LoginComponent },
      { path: "**", redirectTo: "posts" }
    ])
  ],
  providers: [TypicodeService, AuthService, GraphService],
  bootstrap: [AppComponent]
})
export class AppModule {}
