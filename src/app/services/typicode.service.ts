import { Injectable } from '@angular/core';
import { Http } from "@angular/http";
import "rxjs/add/operator/map";

@Injectable()
export class TypicodeService {
  constructor(private http: Http) {}

  getList() {
    return this.http
      .get("https://jsonplaceholder.typicode.com/posts/")
      .map(res => res.json());
  }

  getPhotos(): any {
    return this.http
      .get("https://jsonplaceholder.typicode.com/photos/")
      .map(res => res.json());
  }
}
