import { Component, OnInit } from '@angular/core';
import { TypicodeService } from "./../../services/typicode.service";

@Component({
  selector: "app-photos",
  templateUrl: "./photos.component.html",
  styleUrls: ["./photos.component.css"]
})
export class PhotosComponent implements OnInit {
  photos: any;
  constructor(private typicodeService: TypicodeService) {}

  ngOnInit() {
    this.typicodeService.getPhotos()
      .subscribe(photos => (this.photos = photos));

    console.log();
  }
}
