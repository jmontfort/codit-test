import { Component, OnInit } from '@angular/core';
import { GraphService } from "./../../services/graph.service";
import { AuthService } from "./../../services/auth.service";
@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {
  public user: Msal.User = null;
  public userInfo: any = null;
  public apiCallFailed: boolean;
  public loginFailed: boolean;

  constructor(
    private authService: AuthService,
    private graphService: GraphService
  ) {}

  ngOnInit() {}

  public login() {
    this.loginFailed = false;
    this.authService.login().then(
      user => {
        if (user) {
          this.user = user;
        } else {
          this.loginFailed = true;
        }
      },
      () => {
        this.loginFailed = true;
      }
    );
  }

  private callAPI() {
    this.apiCallFailed = false;
    this.authService.getToken().then(
      token => {
        this.graphService.getUserInfo(token).subscribe(
          data => {
            this.userInfo = data;
          },
          error => {
            console.error(error);
            this.apiCallFailed = true;
          }
        );
      },
      error => {
        console.error(error);
        this.apiCallFailed = true;
      }
    );
  }

  private logout() {
    this.authService.logout();
  }
}
