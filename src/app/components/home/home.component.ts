import { TypicodeService } from './../../services/typicode.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: "home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"]
})
export class HomeComponent implements OnInit {
  posts: any;
  constructor(private typicodeService: TypicodeService) {}

  ngOnInit() {
    this.typicodeService.getList()
      .subscribe(posts => this.posts = posts);  
    }
}
